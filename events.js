const form = document.getElementById('form')
form.addEventListener('submit', (event) => {
  event.preventDefault()
  console.log(event)
})

document.getElementById('input')?.addEventListener('click', e => {
  e.preventDefault()
  console.log(e.currentTarget)
})

Array.from(document.getElementsByTagName('li')).forEach(li => {
  li.addEventListener('click', e => {
    li.remove()
  })
})


const menu = document.getElementById('menu')
document.addEventListener('click', event => {
  if(menu.style.display !== 'block') {
    menu.style.display = 'block'
    menu.style.top = event.pageY + 'px'
    menu.style.left = event.pageX + 'px'
  } else {
    menu.style.display = 'none'
  }
})
menu.addEventListener('click', event => {
  event.stopPropagation()
})
