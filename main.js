const person = {
  firstName: 'David',
  lastName: 'Schwarz',
  address: {
    street: 'iwas',
    city: 'iwas'
  },
  grades: [1, 5, 3, 2, 3, 4, 2, 6, 3],
  get fullName() {
    return this.firstName + ' ' + this.lastName
  },
  set fullName(value) {
    let name = value.split(' ')
    console.log(name)
    this.firstName = name[0]
    this.lastName = name[1]
  },
  avgGrade: 1,
  save() {

  }
}

const evenGrades = person.grades
  .filter(grade => grade % 2 === 0)
  .filter(grade => grade < 5)
  .map(grade => grade * 2)
  .sort()

console.log(evenGrades)

console.log(person.fullName)
console.log(person.save())
person.fullName = 'Max Mustermann'

//const shopItems = [{brand: 'nike', size: 'xl'}, {}]
//const filter = {brand: 'nike', size: 'xl'}
// [["brand", "nike"], ["size", "xl"]]

//Object.entries(filter).reduce((output, ([key, value])) => {
//  return output.filter(item => item[key] === value)
//}, shopItems)
String.prototype.v = function () {
  console.log(this)
  return this + " hi";
}
function doSomething(obj) {
  return obj?.firstName?.v() + obj?.lastName
}
console.log(doSomething(person))

const x = 'null'
//const x = null
const y = x ? 'foo' : 'bar'
console.log(y)
